# Vue DevUI

Vue3版本的DevUI组件库，基于[https://github.com/devcloudfe/ng-devui](https://github.com/devcloudfe/ng-devui)

DevUI官方网站：[https://devui.design](https://devui.design)

Tips: 该项目目前还处于孵化和演进阶段，欢迎大家一起参与建设🤝

目前，组件移植的基本流程已经打通，欢迎大家参与到 Vue DevUI 项目的建设中来！👏🎉

通过参与 Vue DevUI 项目，你可以：
- 学习最新的 Vite+Vue3+TSX 技术
- 学习如何设计和开发组件
- 参与到开源社区中来
- 结识一群热爱学习、热爱开源的朋友

以下是项目进展和规划：
[项目进展和规划](https://gitee.com/devui/vue-devui/wikis/%E9%A1%B9%E7%9B%AE%E8%BF%9B%E5%B1%95%E5%92%8C%E8%A7%84%E5%88%92)

# 快速开始

## 1 安装依赖

```
yarn(推荐)

or

npm i
```

## 2 启动

```
yarn dev(推荐)

or

npx vite

or

npm run dev
```

## 3 访问

[http://localhost:3000/](http://localhost:3000/)

## 4 生产打包

```
yarn build(推荐)

or

npm run build
```

# 贡献者花名册

排名不分先后（按首字母排序）

## asian-TMac

[asian-TMac](https://gitee.com/asian-TMac)

目前负责的责任田有：
- Anchor 锚点

## big_also

[big_also](https://gitee.com/big_also)

目前负责的责任田有：
- Modal 模态框

## binghai1

[binghai1](https://gitee.com/binghai1)

目前负责的责任田有：
- Layout 布局
- Tags 标签

## brenner8023

[brenner8023](https://gitee.com/brenner8023)

目前主要负责：
- Jest 单元测试环境搭建
- Radio 单选框
- CheckBox 复选框
- Switch 开关
- TagsInput 标签输入
- TextInput 文本框
- Popover 悬浮提示

## chengxi_24

[chengxi_24](https://gitee.com/chengxi_24)

目前主要负责：
- EditableSelect 可输入下拉选择框

## Dr_HHH

[Dr_HHH](https://gitee.com/Dr_HHH)

目前主要负责：
- TimePicker 时间选择器

## fayeahli

[fayeahli](https://gitee.com/fayeahli)

目前主要负责：
- Fullscreen 全屏

## flxy1028(fang-ng4)

[flxy1028](https://github.com/flxy1028)

目前负责的责任田有：
- Tabs 选项卡

## foryangxuechen

[foryangxuechen](https://gitee.com/foryangxuechen)

目前负责的责任田有：
- Textarea 多行文本框

## guaimeengmengxiaoxiong

[guaimeengmengxiaoxiong](https://gitee.com/guaimeengmengxiaoxiong)

目前负责的责任田有：
- Transfer 穿梭框

## gxuud

[gxuud](https://gitee.com/gxuud)

目前负责的责任田有：
- Tree 树
- TreeSelect 树形选择框

## hyhup

[hyhup](https://gitee.com/hyhup)

目前负责的责任田有：
- ReadTip 阅读提示

## Jecyu

[Jecyu](https://gitee.com/Jecyu)

目前负责的责任田有：
- DragDrop 拖拽
- Splitter 分割器

## jenson-miao

[jenson-miao](https://gitee.com/jenson-miao)

目前负责的责任田有：
- Status 状态
- Time Axis 时间轴

## johhny_idowe

[johhny_idowe](https://gitee.com/johhny_idowe)

目前负责的责任田有：
- DataTable 表格

## kagol

[kagol](https://github.com/kagol)

目前负责的责任田有：
- Accordion 手风琴

## kd554246839

[kd554246839](https://gitee.com/kd554246839)

目前负责的责任田有：
- Pagination 分页
- Loading 加载

## lao--hu

[lao--hu](https://gitee.com/lao--hu)

目前负责的责任田有：
- Progress 进度条

## lihuiwang

[lihuiwang](https://gitee.com/lihuiwang)

目前负责的责任田有：
- Gantt 甘特图

## lookforwhat

[lookforwhat](https://gitee.com/lookforwhat)

目前负责的责任田有：
- Select 下拉选择框

## lwl(laiweilun)

[lwl](https://gitee.com/laiweilun)

目前负责的责任田有：
- Cascader 级联菜单
- Search 搜索
- CategorySearch 分类搜索

## maizhiyuan

[maizhiyuan](https://gitee.com/maizhiyuan)

目前负责的责任田有：
- Sticky 便贴

## mewcoder

[mewcoder](https://gitee.com/mewcoder)

目前负责的责任田有：
- DatePickerPro 日期选择器

## mrundef

[mrundef](https://gitee.com/mrundef)

目前负责的责任田有：
- DatePicker 日期选择器

## nowisfuture

[nowisfuture](https://gitee.com/nowisfuture)

目前负责的责任田有：
- MultiAutoComplete 多项自动补全
- Quadrant Diagram 象限图

## onlyoupon

[onlyoupon](https://gitee.com/onlyoupon)

目前负责的责任田有：
- Carousel 走马灯

## RootWater(lel)

[RootWater](https://gitee.com/RootWater)

目前负责的责任田有：
- Toast 全局通知

## sise209(duqingyu)

[sise209](https://gitee.com/sise209)

目前负责的责任田有：
- Badge 徽标
- ImagePreview 图片预览

## to0simple(NAN)

[to0simple](https://github.com/to0simple)

目前负责的责任田有：
- Alert 警告
- Avatar 头像
- Panel 面板
- Codebox 代码盒子
- Highlight 代码高亮

## vergil_lu(Vergil)

[vergil_lu](https://gitee.com/vergil_lu)

目前负责的责任田有：
- Card 卡片

## waittutu

[waittutu](https://gitee.com/waittutu)

目前负责的责任田有：
- AutoComplete 自动补全

## wangy0311

[wangy0311](https://gitee.com/wangy0311)

目前负责的责任田有：
- DropDown 下拉菜单

## xiao_jius_father(小九九的爸爸)

[xiao_jius_father](https://gitee.com/xiao_jius_father)

目前负责的责任田有：
- Slider 滑动输入条
- Tooltip 提示

## xyantelope

[xyantelope](https://gitee.com/xyantelope)

目前负责的责任田有：
- Drawer 抽屉板

## yonsunzhen

[yonsunzhen](https://gitee.com/yonsunzhen)

目前负责的责任田有：
- BackTop 回到顶部
- StepsGuide 操作指引

## yqsheng

[yqsheng](https://gitee.com/yqsheng)

目前负责的责任田有：
- InputNumber 数字输入框

## YuanZiWRY

[YuanZiWRY](https://gitee.com/YuanZiWRY)

目前负责的责任田有：
- Menu 导航
- Breadcrumb 面包屑

## Zcating

[Zcating](https://github.com/Zcating)

目前负责的责任田有：
- Button 按钮

## zzzautumn(~zZ.Lucky)

[zzzautumn](https://gitee.com/zzzautumn)

目前负责的责任田有：
- Rate 等级评估
- Upload 上传
